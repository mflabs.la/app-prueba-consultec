import { Component, OnInit } from '@angular/core';
import { Service } from '../services/service.module';
import { UtilService } from '../services/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crud',
  templateUrl: 'crud.page.html',
  styleUrls: ['crud.page.scss']
})
export class CrudPage implements OnInit {

  customers:any = [];
  cities:any = [];
  customer:any;
  search = '*';

  constructor(public router: Router, public service:Service, public utilService: UtilService) {

  }

  ngOnInit(){
    this.utilService.presentLoadingWithOptions('bubbles', 2000, 'Espera un momento porfavor');
    this.getCustomer();
    this.getCities();
  }

  getCustomer(){
     this.service.getAllWhere('customers', 'state', '==', 'Activo' ).subscribe(data =>{
       this.customers = data;
    })
  }
  getCities(){
    this.service.getAll('cities').subscribe(data => { 
      this.cities = data
    });
  }

  viewCustomer(c){
    this.customer = c;
    this.router.navigate(['/view'], this.customer);
  }

  

}
