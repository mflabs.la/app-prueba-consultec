import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms'
import { Service } from '../services/service.module';
import { Customer } from '../models/customer';
import { UtilService } from '../services/util.service';
import { Router, ActivatedRoute} from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-customer-add',
  templateUrl: 'customer-add.page.html',
  styleUrls: ['customer-add.page.scss']
})
export class CustomerAddPage implements OnInit {

  myForm: FormGroup;
  status:any = [];
  cities:any = [];
  customer:any;
  edit:boolean;
  

  constructor(private fb: FormBuilder, 
    private service: Service, 
    private utilService: UtilService, 
    private router: Router,
    private route: ActivatedRoute,
    private navCtrl: NavController) {
   
    this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
    });

 
    }

  ngOnInit(){
    this.getStatus();
    this.getCities();
    this.route.queryParams.subscribe(params => { 
      this.customer = this.router.getCurrentNavigation().extras;
      console.log(this.customer)
      if(this.customer.edit == 1){
        this.edit = true;
        this.myForm.get("name").setValue(this.customer.name)
        this.myForm.get("surname").setValue(this.customer.surname)
        this.myForm.get("email").setValue(this.customer.email)
        this.myForm.get("state").setValue(this.customer.state)
        this.myForm.get("city").setValue(this.customer.city)
      }else{
        this.customer = new Customer(); 
        this.edit = false;
      }
    }); 
  }

  getStatus(){
    this.service.getAll('status').subscribe(data => {this.status = data} );
  } 
  
  getCities(){
    this.service.getAll('cities').subscribe(data => { this.cities = data});
  }


  saveData(){
    if(this.myForm && this.myForm.invalid)
      return 
      this.customer.name = this.myForm.value.name;
      this.customer.surname = this.myForm.value.surname;
      this.customer.email = this.myForm.value.email;
      this.customer.state = this.myForm.value.state;
      this.customer.city = this.myForm.value.city;
      if(!this.edit){
        this.service.saveOrUpdate("customers", this.customer).then(data =>{
          this.utilService.presentToast('customer information has been saved successfully', 2000);
          this.edit = true;
          this.router.navigate(['/tabs/crud']);
        }).catch((error => {
          
        }));
      }else{
        this.service.saveOrUpdate("customers", this.customer, false).then(data =>{
          this.utilService.presentToast('has been edited correctly', 2000);
          this.edit = false;
          this.router.navigate(['/tabs/crud']);
        }).catch((error => {
          
        }));
      }  
  }


  
  bacK(){
    this.navCtrl.navigateRoot(['/tabs/crud']);
  }

}
