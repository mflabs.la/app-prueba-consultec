import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Service } from '../services/service.module';
import { UtilService } from '../services/util.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-customer-view',
  templateUrl: 'customer-view.page.html',
  styleUrls: ['customer-view.page.scss']
})
export class CustomerViewPage implements OnInit {

  customer:any;
  edit:boolean = false;

  constructor(private router: Router, 
    private service: Service, 
    private utilService: UtilService,
    private alertController: AlertController,
    private route: ActivatedRoute) {
  }

  ngOnInit(){
    this.route.queryParams.subscribe(params => { 
         this.customer = this.router.getCurrentNavigation().extras; 
       });
  }

  async disableCustomer(customer){
      const alert = await this.alertController.create({
        header: 'are you sure to remove this client?',
        buttons: [
          {
            text: 'Cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              this.customer = customer
              this.customer.state = 'Inactivo';
              this.service.saveOrUpdate("customers", this.customer, false).then(data =>{
                this.utilService.presentToast('the customer has been removed', 2000);
                this.router.navigate(['/tabs/crud']);
              }).catch((error => {
                
              }));
            }
          }
        ]
      });
    await alert.present();
  }

  goEditCustomer(customer){
    this.customer = customer;
    this.customer.edit = 1
    this.router.navigate(['/add'], this.customer );
  }

  
}
