import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UtilService } from '../services/util.service';
import { Service } from '../services/service.module';

declare var google;

@Component({
  selector: 'app-location',
  templateUrl: 'location.page.html',
  styleUrls: ['location.page.scss']
})
export class LocationPage implements OnInit{

  mapRef = null;
  markers:any = [];
  branchOffices:any = [];
  myLatlng:any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  waypoint: any = {
    lat : '',
    lng : ''
  };

  infoWindow = new google.maps.InfoWindow({
    content:" "
  });

  constructor(private geolocation: Geolocation, 
              private utilService: UtilService,
              private service: Service) {
                this.directionsService = new google.maps.DirectionsService();
                this.directionsDisplay = new google.maps.DirectionsRenderer();
                this.bounds = new google.maps.LatLngBounds();
              }
                
  ngOnInit() {
    this.getBranchOffices();
  }

  
  async loadMap(){
    this.utilService.presentLoadingWithOptions('bubbles', 2000, 'Espera un momento porfavor');
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    this.myLatlng = await this.getGeolocation();
    // create map
    this.mapRef = new google.maps.Map(mapEle, {
      center: this.myLatlng,
      zoom: 16
    });

    new google.maps.Marker({
      position: this.myLatlng,
      map: this.mapRef,
      icon: 'assets/icon/map-icon-p.png',
    });

    this.directionsDisplay.setMap(this.mapRef);
  
    google.maps.event.addListenerOnce(this.mapRef, 'idle', () => {
      for(let p of this.branchOffices){
        console.log(p)
        let marker = new google.maps.Marker({
          position: {lat: parseFloat(p.lat), lng: parseFloat(p.lng)},
          map: this.mapRef,
          icon: 'assets/icon/map-icon-v.png',
        });
        marker.addListener('click', e => {
          this.waypoint = {lat: e.latLng.lat(), lng: e.latLng.lng()};
          this.mapRef.panTo(e.latLng);
          for (let m of this.markers)
            m.setIcon('assets/icon/map-icon-v.png');
            marker.setIcon('assets/icon/map-icon-p.png');
            this.calculateRoute();
        });
        this.markers.push(marker)
      }
    });
  }

  private calculateRoute(){
    
    var point = new google.maps.LatLng(this.myLatlng.lat, this.myLatlng.lng);
    this.bounds.extend(point);
    var point2 = new google.maps.LatLng(this.waypoint.lat, this.waypoint.lng);
    this.bounds.extend(point2);

    this.mapRef.fitBounds(this.bounds);
    this.directionsService.route({
      origin: new google.maps.LatLng(this.myLatlng.lat, this.myLatlng.lng),
      destination: new google.maps.LatLng(this.waypoint.lat, this.waypoint.lng),
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response, status)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        console.log(response);
        this.directionsDisplay.setDirections(response);
      }else{
        alert('Could not display directions due to: ' + status);
      }
    });  
  }

  private async getGeolocation (){
    const resp = await this.geolocation.getCurrentPosition();
    return {
      lat: resp.coords.latitude,
      lng: resp.coords.longitude
    };
  }

  getBranchOffices(){
    this.service.getAll(' branchOffices').subscribe(data =>{
      this.branchOffices = data;
      this.loadMap();
    })
  }


}
