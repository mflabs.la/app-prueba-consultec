import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable()

export class Service {

  routes:any;
  user:any;

  constructor( private db: AngularFirestore ) {
   }

  getAll( collection ) {      
      let ref = this.db.collection('/' + collection);    
      return ref.snapshotChanges().pipe(map(actions => {
        return actions.map(a => { 
          const data = a.payload.doc.data() as any;
          const id = a.payload.doc.id;
          return { id, ...data };  
        }); 
      }));
  }

  getAllWhere(collection, property,  expression, value){
    let ref =  this.db.collection(collection, ref => ref.where(property, expression, value));
    return ref.snapshotChanges().pipe(map(actions => {
      return actions.map(a => { 
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };  
      }); 
    }));
  }


  getById(collection, id) {
    return  this.db.doc( '/' + collection + '/' + id);
  }

  saveOrUpdate(collection, data, save = true) {
    const that = this;
    if (save) {
      return new Promise(function(resolve, reject){
        const id = that.db.createId();
        const itemObservable = that.db.doc('/' + collection + '/' + id);
        itemObservable.set(Object.assign({},JSON.parse(JSON.stringify( data )))).then(() => resolve (true)).catch(error =>  reject (error))
      })
    } else {
      return new Promise(function(resolve, reject){
        let object  : AngularFirestoreDocument<any> =  that.db.collection('/' + collection + '/').doc(data.id);
        object.set(Object.assign({},JSON.parse(JSON.stringify( data ))), {merge: true}).then(() => resolve (true)).catch(error =>  reject (error))
      })
    }
  }

  delete(collection, id) {
    let that = this;
		return new Promise(function(resolve, reject){
			that.db.doc('/' + collection + '/'  + id).delete();
			resolve (true)
		})
  }

  documentToDomainObject = _ => { 
    const object = _.payload.doc.data();
    object.key = _.payload.doc.id; 
    return object; }
  
} 
