import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';


@Injectable()

export class UtilService {

  constructor( public toastController: ToastController, 
    public loadingController: LoadingController, 
    public popoverController: PopoverController ) {
   }

   async presentToast(msg, durtn) {
    const toast = await this.toastController.create({
      message: msg,
      duration: durtn
    });
    toast.present();
  }

  async presentLoadingWithOptions(spinner, durtn, msg) {
    const loading = await this.loadingController.create({
    /*spinners:  "bubbles" | "circles" | "crescent" | "dots" | "lines" | "lines-small" | null | undefined */
      spinner: spinner,
      duration: durtn,
      message:  msg,
    });
    return await loading.present();
  }

  async presentPopover(ev: any, component) {
    const popover = await this.popoverController.create({
      component: component,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  
 
 
  
} 









