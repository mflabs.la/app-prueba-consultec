import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'login', children: [ { path: '', loadChildren: () =>
              import('../login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      {
        path: 'location', children: [ {  path: '', loadChildren: () =>
              import('../location/location.module').then(m => m.LocationPageModule)
          }
        ]
      },
      {
        path: 'crud', children: [ { path: '', loadChildren: () =>
              import('../crud/crud.module').then(m => m.CrudPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/location',
        pathMatch: 'full'
      },
      {
        path: '',
        redirectTo: '/tabs/crud',
        pathMatch: 'full'
      },
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/login',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
