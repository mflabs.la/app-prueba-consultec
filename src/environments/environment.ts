// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // Your web app's Firebase configuration
  firebase: {
    apiKey: "AIzaSyDY2JLfug-PrYxI2Z33tgV8LWezGNNP7ao",
    authDomain: "consultec-cd026.firebaseapp.com",
    databaseURL: "https://consultec-cd026.firebaseio.com",
    projectId: "consultec-cd026",
    storageBucket: "consultec-cd026.appspot.com",
    messagingSenderId: "964753805842",
    appId: "1:964753805842:web:d3b9c48ef403bfb7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
